// package br.com.challenge.users.controller;

// import static org.hamcrest.CoreMatchers.equalTo;
// import static org.hamcrest.CoreMatchers.notNullValue;
// import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
// import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
// import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
// import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
// import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
// import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
// import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
// import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;

// import java.time.LocalDate;

// import org.junit.Before;
// import org.junit.Test;
// import org.springframework.beans.factory.annotation.Autowired;

// import br.com.challenge.users.util.UtilTest;
// import br.com.modules.users.dto.UserDTO;
// import br.com.modules.users.repository.IUserRepository;

// public class UsersControllerTest extends UtilTest {
	
// 	private final String baseUrl = "/users";
	
// 	@Autowired
// 	private IUserRepository repository;
	
// 	private UserDTO userForTest1;
// 	private UserDTO userForTest2;
// 	private UserDTO userForTest3;

// 	@Before
// 	public void setup() throws Exception {
// 		this.mockMvc = webAppContextSetup(webApplicationContext).build();
		
// 		this.userForTest1 = new UserDTO("user.teste1@email.com", LocalDate.of(1980,12,03), 1);
// 		this.userForTest2 = new UserDTO("user.teste2@email.com", LocalDate.of(1980,12,03), 2);
// 		this.userForTest3 = new UserDTO("user.teste3@email.com", LocalDate.of(1980,12,03), 1);
		
// 		this.userForTest2 = new UserDTO(repository.save(userForTest2.toEntity()));
// 		this.userForTest3 = new UserDTO(repository.save(userForTest3.toEntity()));
// 	}
	
// 	@Test
// 	public void createUser() throws Exception {
// 		String jsonObject = json(this.userForTest1);
		
// 		this.mockMvc.perform(post(baseUrl)
// 				.contentType(contentType).content(jsonObject))
// 				.andExpect(status().isOk())
// 				.andExpect(jsonPath("$.userId", notNullValue()))
// 				.andExpect(jsonPath("$.email", equalTo(userForTest1.getEmail())))
// 				.andExpect(jsonPath("$.birthdate", equalTo(dateToString(userForTest1.getBirthdate()))))
// 				.andExpect(jsonPath("$.companyId", equalTo(userForTest1.getCompanyId())));
// 	}

// 	@Test
// 	public void getOne() throws Exception {	
// 		this.mockMvc.perform(get(baseUrl + "/" + userForTest2.getUserId()))
// 			.andExpect(content().contentType(contentType))
// 			.andExpect(status().isOk())
// 			.andExpect(jsonPath("$.userId", notNullValue()))
// 			.andExpect(jsonPath("$.email", equalTo(userForTest2.getEmail())))
// 			.andExpect(jsonPath("$.birthdate", equalTo(dateToString(userForTest2.getBirthdate()))))
// 			.andExpect(jsonPath("$.companyId", equalTo(userForTest2.getCompanyId())));
// 	}
	
// 	@Test
// 	public void deleteUser() throws Exception {
// 		this.mockMvc.perform(delete(baseUrl + "/" + userForTest3.getUserId())
// 			.contentType(contentType))
// 			.andExpect(status().isOk())
// 			.andExpect(jsonPath("$.userId", notNullValue()))
// 			.andExpect(jsonPath("$.email", equalTo(userForTest3.getEmail())))
// 			.andExpect(jsonPath("$.birthdate", equalTo(dateToString(userForTest3.getBirthdate()))))
// 			.andExpect(jsonPath("$.companyId", equalTo(userForTest3.getCompanyId())));
// 	}
	
// 	@Test
// 	public void updateUser() throws Exception {
// 		userForTest2.setEmail("new@email.com");
// 		userForTest2.setBirthdate(LocalDate.of(1988,10,01));

// 		String jsonObject = json(userForTest2);

// 		this.mockMvc.perform(put(baseUrl + "/" + userForTest2.getUserId())
// 			.contentType(contentType)
// 			.content(jsonObject))
// 			.andExpect(status().isOk())
// 			.andExpect(jsonPath("$.userId", notNullValue()))
// 			.andExpect(jsonPath("$.email", equalTo(userForTest2.getEmail())))
// 			.andExpect(jsonPath("$.birthdate", equalTo(dateToString(userForTest2.getBirthdate()))))
// 			.andExpect(jsonPath("$.companyId", equalTo(userForTest2.getCompanyId())));
// 	}
	
// 	@Test
// 	public void createUserWithSameEmail() throws Exception {
// 		UserDTO userOne = new UserDTO("user.teste5@email.com", LocalDate.of(1980,12,03), 1);
// 		UserDTO userTwo = new UserDTO("user.teste5@email.com", LocalDate.of(1980,12,03), 1);
// 		repository.save(userOne.toEntity());
		
// 		String jsonObject = json(userTwo);
		
// 		this.mockMvc.perform(post(baseUrl)
// 				.contentType(contentType).content(jsonObject))
// 				.andExpect(status().isConflict());
// 	}

// 	@Test
// 	public void getNonexistentUser() throws Exception {
// 		this.mockMvc.perform(get(baseUrl + "/" + 999999))
// 			.andExpect(content().contentType(contentType))
// 			.andExpect(status().isNotFound());
// 	}
	
// 	@Test
// 	public void createUserWithNullParameters() throws Exception {
// 		UserDTO user = new UserDTO(); 
// 		String jsonObject = json(user);
		
// 		this.mockMvc.perform(post(baseUrl)
// 				.contentType(contentType).content(jsonObject))
// 				.andExpect(status().isBadRequest());
// 	}
	
// 	@Test
// 	public void createUserWithInvalidCompanyId() throws Exception {
// 		UserDTO user = new UserDTO("user.teste4@email.com", LocalDate.of(1980,12,03), 999);

// 		String jsonObject = json(user);
		
// 		this.mockMvc.perform(post(baseUrl)
// 				.contentType(contentType).content(jsonObject))
// 				.andExpect(status().isBadRequest());
// 	}
// }
