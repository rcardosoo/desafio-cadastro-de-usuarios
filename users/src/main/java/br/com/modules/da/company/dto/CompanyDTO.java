package br.com.modules.da.company.dto;

import java.util.UUID;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;

import br.com.modules.da.company.enumeration.CompanyStatus;
import br.com.modules.da.company.model.Company;
import br.com.modules.da.mapper.MapperConverter;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class CompanyDTO {
	
    private UUID identifier;
    
    @NotBlank
    private String name;
    
    private CompanyStatus status;
    
    private String clientURL;
    
    @Email
    private String notificationEmail;

    public static CompanyDTO toDto(Company entity) {
    	return MapperConverter.convert(entity, CompanyDTO.class);
    }
    
    public Company toEntity() {
    	return MapperConverter.convert(this, Company.class);
    }
}
