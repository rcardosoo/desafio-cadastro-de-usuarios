package br.com.modules.da.user.service.impl;

import java.util.HashSet;
import java.util.Set;

import org.springframework.stereotype.Service;

import br.com.modules.da.resource.dto.ResourceDTO;
import br.com.modules.da.resource.model.Resource;
import br.com.modules.da.resource.service.IResourceService;
import br.com.modules.da.user.service.IUserResourceLinkingService;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
@Service
@RequiredArgsConstructor
public class UserResourceLinkingServiceImpl implements IUserResourceLinkingService {
	
	@NonNull private final IResourceService resourceService;
	
	@Override
	public Set<Resource> vinculateResourceEntityList(Set<ResourceDTO> dtoList) {
		Set<Resource> resources = new HashSet<>();
		dtoList.forEach(dto -> 
		resources.add(resourceService.getOne(dto.getIdentifier().toString()).toEntity())
		);
		return resources;
	}

}
