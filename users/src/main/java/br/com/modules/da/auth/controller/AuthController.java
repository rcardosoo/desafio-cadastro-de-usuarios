package br.com.modules.da.auth.controller;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.modules.da.auth.dto.AuthCredentialsDTO;
import br.com.modules.da.auth.dto.AuthResponseDTO;
import br.com.modules.da.auth.service.IAuthService;

@RestController
@RequestMapping("/auth")
public class AuthController {
	
	private final IAuthService service;

	public AuthController(IAuthService service) {
		this.service = service;
	}
	
    @PostMapping()
	public ResponseEntity<AuthResponseDTO> authenticate(
			@Valid @RequestBody AuthCredentialsDTO credentials,
			HttpServletResponse response) {
    	
		return ResponseEntity.ok(service.authenticate(credentials, response));
	}
}
