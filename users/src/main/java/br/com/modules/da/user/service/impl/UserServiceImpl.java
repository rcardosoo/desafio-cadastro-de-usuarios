package br.com.modules.da.user.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import br.com.modules.da.company.dto.CompanyDTO;
import br.com.modules.da.company.service.ICompanyService;
import br.com.modules.da.exception.NotFoundException;
import br.com.modules.da.user.dto.UserDTO;
import br.com.modules.da.user.model.User;
import br.com.modules.da.user.repository.IUserRepository;
import br.com.modules.da.user.service.IUserGroupLinkingService;
import br.com.modules.da.user.service.IUserResourceLinkingService;
import br.com.modules.da.user.service.IUserService;
import br.com.modules.da.util.Constants;
import br.com.modules.da.util.UUIDUtils;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class UserServiceImpl implements IUserService {
	
	@NonNull private final IUserRepository repository;
	@NonNull private final BCryptPasswordEncoder encoder;
	@NonNull private final ICompanyService companyService;
	@NonNull private final IUserGroupLinkingService userGroupLinkingService;
	@NonNull private final IUserResourceLinkingService userResourceLinkingService;

	@Override
	public UserDTO create(UserDTO userDTO) {
		User user = userDTO.toEntity();
		
		CompanyDTO company = companyService
				.getOne(userDTO.getCompany().getIdentifier().toString());
		
		user.setCompany(company.toEntity());
		
		user.setGroups(userGroupLinkingService
				.vinculateGroupEntityList(userDTO.getGroups()));
		
		user.setResources(userResourceLinkingService
				.vinculateResourceEntityList(userDTO.getResources()));
		
		user.setPassword(encoder.encode(userDTO.getPassword()));
		return UserDTO.toDto(repository.save(user));
	}

	@Override
	public UserDTO getOne(String id) {
		UUID identifier = UUIDUtils.validateString(id);
		
		return repository.findById(identifier).map(UserDTO::toDto)
				.orElseThrow(() -> new NotFoundException(Constants.NOT_FOUND_USER));
	}

	@Override
	public UserDTO update(String id, UserDTO userDTO) {
		UUID identifier = UUIDUtils.validateString(id);

		return repository.findById(identifier).map(user -> {
			user.setName(userDTO.getName());
			user.setAddress(userDTO.getAddress().toEntity());
			user.setContact(userDTO.getContact().toEntity());
			user.setStatus(userDTO.getStatus());
			
			user.setGroups(userGroupLinkingService
					.vinculateGroupEntityList(userDTO.getGroups()));
			
			user.setResources(userResourceLinkingService
					.vinculateResourceEntityList(userDTO.getResources()));
			
			return UserDTO.toDto(repository.save(user));
		}).orElseThrow(() -> new NotFoundException(Constants.NOT_FOUND_USER));
	}

	@Override
	public UserDTO delete(String id) {
		UUID identifier = UUIDUtils.validateString(id);

		return repository.findById(identifier).map(user -> {
			repository.deleteById(identifier);
			return UserDTO.toDto(user);
        }).orElseThrow(() -> new NotFoundException(Constants.NOT_FOUND_USER));
	}

	@Override
	public Page<UserDTO> getAll(Pageable pageable) {
        List<UserDTO> dtoList = new ArrayList<>();
        Page<User> pageResult = repository.findAll(pageable);
        pageResult.forEach(user -> dtoList.add(UserDTO.toDto(user)));
		return new PageImpl<>(dtoList, pageable, pageResult.getTotalElements());
	}

}
