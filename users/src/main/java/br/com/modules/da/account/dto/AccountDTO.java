package br.com.modules.da.account.dto;

import java.math.BigDecimal;
import java.util.UUID;

import javax.validation.constraints.Digits;
import javax.validation.constraints.NotNull;

import br.com.modules.da.account.model.Account;
import br.com.modules.da.mapper.MapperConverter;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class AccountDTO {

	private UUID identifier;

	@NotNull
	@Digits(integer=10, fraction=2)
	private BigDecimal availableBalance;

	@NotNull
	@Digits(integer=10, fraction=2)
	private BigDecimal blockedBalance;	

	public static AccountDTO toDto(Account entity) {
		return MapperConverter.convert(entity, AccountDTO.class);
	}

	public Account toEntity() {
		return MapperConverter.convert(this, Account.class);
	}
}
