package br.com.modules.da.util;

import org.springframework.data.domain.Page;
import org.springframework.http.HttpHeaders;

public class PageHeaders extends HttpHeaders {
	
	private static final long serialVersionUID = 1L;

	public PageHeaders(Page<?> page) {
        add("Page", String.valueOf(page.getPageable().getPageNumber()));
        add("TotalPages", String.valueOf(page.getTotalPages()));
        add("TotalElements", String.valueOf(page.getTotalElements()));
        add("TotalElementsOnPage", String.valueOf(page.getNumberOfElements()));
        add("ElementsPerPage", String.valueOf(page.getPageable().getPageSize()));
    }
}
