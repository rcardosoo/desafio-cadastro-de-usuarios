package br.com.modules.da.user.service;

import java.util.Set;

import br.com.modules.da.resource.dto.ResourceDTO;
import br.com.modules.da.resource.model.Resource;

public interface IUserResourceLinkingService {
	
	Set<Resource> vinculateResourceEntityList(Set<ResourceDTO> dtoList);
}
