package br.com.modules.da.security.filter;

import java.io.IOException;
import java.util.Collections;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.core.env.Environment;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.GenericFilterBean;

import br.com.modules.da.exception.AuthExceptionEntryPoint;
import br.com.modules.da.security.service.ISecurityService;
import br.com.modules.da.util.Constants;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.MalformedJwtException;
import io.jsonwebtoken.SignatureException;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@Component
@RequiredArgsConstructor
public class JWTAuthenticationFilter extends GenericFilterBean {
	
	@NonNull private final ISecurityService securityService;
	
	@NonNull private final Environment env;

	@Override
	public void doFilter(ServletRequest req, ServletResponse resp, FilterChain chain)
			throws IOException, ServletException {
		HttpServletRequest request = (HttpServletRequest) req;
		HttpServletResponse response = (HttpServletResponse) resp;
		AuthExceptionEntryPoint handlerException = new AuthExceptionEntryPoint();
		Authentication authentication = null;
		
		String token = request.getHeader(Constants.HEADER_STRING_AUTHORIZATION);
		
		try {
			if (token != null) {
				Claims claims = securityService.decryptAuthorizationToken(env.getProperty("module.security.secret-key"), token);
				String user = claims.getSubject();
				
				if (user != null) {
					authentication = new UsernamePasswordAuthenticationToken(user, null, Collections.emptyList());
				}
			}
			SecurityContextHolder.getContext().setAuthentication(authentication);
			chain.doFilter(req, resp);			
		} catch (ExpiredJwtException e) {
			handlerException.commence(response, new Exception(Constants.EXPIRED_TOKEN));
		} catch (MalformedJwtException | SignatureException e) {
			handlerException.commence(response, new Exception(Constants.INVALID_TOKEN));
		}		
	}

}
