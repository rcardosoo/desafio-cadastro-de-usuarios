package br.com.modules.da.auth.dto;

import java.util.Set;
import java.util.UUID;

import br.com.modules.da.group.dto.GroupDTO;
import br.com.modules.da.resource.dto.ResourceDTO;
import br.com.modules.da.user.model.User;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder(builderMethodName = "builder")
public class AuthResponseDTO {
	
	private UUID userIdentifier;
	
	private UUID companyIdentifier;
	
	private Set<GroupDTO> group;
	
	private Set<ResourceDTO> exceptionPermissions;
	
    public static AuthResponseDTO toDto(User user) {
    	return AuthResponseDTO.builder()
    			.userIdentifier(user.getIdentifier())
    			.companyIdentifier(user.getCompany().getIdentifier())
    			.group(GroupDTO.toDtoList(user.getGroups()))
    			.exceptionPermissions(ResourceDTO.toDtoList(user.getResources()))
    			.build();
    }
}
