package br.com.modules.da.recovery.service.impl;

import org.springframework.stereotype.Service;

import br.com.modules.da.exception.BadRequestException;
import br.com.modules.da.recovery.enumeration.RecoveryPasswordStatus;
import br.com.modules.da.recovery.model.RecoveryPassword;
import br.com.modules.da.recovery.repository.IRecoveryPasswordRepository;
import br.com.modules.da.recovery.service.IRecoveryActionStrategy;
import br.com.modules.da.util.Constants;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Service("recoveryActionExpired")
public class RecoveryActionExpiredTokenImpl implements IRecoveryActionStrategy {
	
	@NonNull private final IRecoveryPasswordRepository repository;
	
	@Override
	public void recoveryAction(RecoveryPassword recovery, String password) {
		recovery.setStatus(RecoveryPasswordStatus.EXPIRED);
		repository.save(recovery);
		throw new BadRequestException(Constants.EXPIRED_RECOVERY_TOKEN);
	}

}
