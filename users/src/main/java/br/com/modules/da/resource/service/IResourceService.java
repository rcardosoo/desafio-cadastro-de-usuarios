package br.com.modules.da.resource.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import br.com.modules.da.resource.dto.ResourceDTO;

public interface IResourceService {
	
	ResourceDTO create(ResourceDTO resourceDTO);
	
	ResourceDTO getOne(String id);
	
	Page<ResourceDTO> getAll(Pageable pageable);
	
	ResourceDTO update(String id, ResourceDTO resourceDTO);
	
	ResourceDTO delete(String id);
}
