package br.com.modules.da.company.repository;

import java.util.UUID;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.modules.da.company.model.Company;

@Repository
public interface ICompanyRepository extends JpaRepository<Company, UUID> {
	
}

