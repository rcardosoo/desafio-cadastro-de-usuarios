package br.com.modules.da.recovery.service;

import java.util.Locale;

import br.com.modules.da.recovery.dto.LinkCredentialsDTO;

public interface IGenerateRecoveryLinkService {
	
	void generateLink(LinkCredentialsDTO credentials, Locale local);

}
