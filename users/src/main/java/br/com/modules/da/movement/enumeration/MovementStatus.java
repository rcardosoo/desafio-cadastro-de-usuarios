package br.com.modules.da.movement.enumeration;

public enum MovementStatus {
	
	PENDING,
	PROCESSED,
	DENIED;
}
