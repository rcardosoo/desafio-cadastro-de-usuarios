package br.com.modules.da.security.service;

import io.jsonwebtoken.Claims;

public interface ISecurityService {
	
	Claims decryptAuthorizationToken(String secretKey, String token);
	
	String generateAuthorizationToken(String subject, String secret, long expirationTime);
}
