package br.com.modules.da.group.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import br.com.modules.da.group.dto.GroupDTO;

public interface IGroupService {
	
	GroupDTO create(GroupDTO groupDTO);
	
	GroupDTO getOne(String id);
	
	Page<GroupDTO> getAll(Pageable pageable);
	
	GroupDTO update(String id, GroupDTO groupDTO);
	
	GroupDTO delete(String id);
}
