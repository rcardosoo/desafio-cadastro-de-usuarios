package br.com.modules.da.recovery.service.impl;

import java.util.Locale;
import java.util.Optional;
import java.util.UUID;

import org.springframework.stereotype.Service;

import br.com.modules.da.company.dto.CompanyDTO;
import br.com.modules.da.company.service.ICompanyService;
import br.com.modules.da.exception.BadRequestException;
import br.com.modules.da.recovery.dto.LinkCredentialsDTO;
import br.com.modules.da.recovery.enumeration.RecoveryPasswordStatus;
import br.com.modules.da.recovery.model.RecoveryPassword;
import br.com.modules.da.recovery.repository.IRecoveryPasswordRepository;
import br.com.modules.da.recovery.service.IGenerateRecoveryLinkService;
import br.com.modules.da.recovery.service.IRecoveryMailService;
import br.com.modules.da.user.dto.UserDTO;
import br.com.modules.da.user.service.IUserQueryService;
import br.com.modules.da.util.Constants;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class GenerateRecoveryLinkServiceImpl implements IGenerateRecoveryLinkService {
	
	@NonNull private final IUserQueryService userQueryService;
	@NonNull private final IRecoveryPasswordRepository repository;
	@NonNull private final IRecoveryMailService recoveryMailService;
	@NonNull private final ICompanyService companyService;

	@Override
	public void generateLink(LinkCredentialsDTO credentials, Locale locale) {
		UserDTO user = userQueryService.byEmail(credentials.getEmail());
		RecoveryPasswordStatus pending = RecoveryPasswordStatus.PENDING;
		UUID token = UUID.randomUUID();
		this.validateRecoveryFromUserCredentils(user, pending);
		CompanyDTO company = companyService.getOne(user.getCompany().getIdentifier().toString());

		String link = company.getClientURL() + token.toString();

		RecoveryPassword recovery = RecoveryPassword.builder()
			.userEmail(user.getEmail())
			.userIdentifier(user.getIdentifier())
			.status(pending)
			.token(token.toString())
			.link(link)
			.build();
		
		recoveryMailService.sendRecoveryMail(recovery, user, company, locale);
	}
	
	private void validateRecoveryFromUserCredentils(UserDTO user, RecoveryPasswordStatus status) {
		Optional<RecoveryPassword> recovery = repository.findByUserIdentifierAndStatus(user.getIdentifier(), status.name());
		
		if (recovery.isPresent()) {
			throw new BadRequestException(Constants.RECOVERY_EMAIL_CONFLICT);
		}
	}

}
