package br.com.modules.da.resource.repository;

import java.util.UUID;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.modules.da.resource.model.Resource;

@Repository
public interface IResourceRepository extends JpaRepository<Resource, UUID> {
	
}