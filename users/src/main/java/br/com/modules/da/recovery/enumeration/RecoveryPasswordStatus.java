package br.com.modules.da.recovery.enumeration;

public enum RecoveryPasswordStatus {
	
	CHANGED,
	EXPIRED,
	PENDING;
}
