package br.com.modules.da.recovery.model;

import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import br.com.modules.da.audit.model.Audit;
import br.com.modules.da.recovery.enumeration.RecoveryPasswordStatus;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "TB_RECOVERY_PASSWORD")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder(builderMethodName = "builder")
public class RecoveryPassword extends Audit {
	
	@Id
	@GeneratedValue
	@Column(columnDefinition = "BINARY(16)")
	private UUID identifier;
	
	private String userEmail;
	
	private UUID userIdentifier;
	
    @Enumerated(EnumType.STRING)
    @Column(length = 20)
	private RecoveryPasswordStatus status;
	
	private String token;
	
	@Transient
	private String link;
}
