package br.com.modules.da.company.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import br.com.modules.da.company.dto.CompanyDTO;

public interface ICompanyService {
	
	CompanyDTO create(CompanyDTO companyDTO);
	
	CompanyDTO getOne(String id);
	
	Page<CompanyDTO> getAll(Pageable pageable);
	
	CompanyDTO update(String id, CompanyDTO companyDTO);
	
	CompanyDTO delete(String id);
}

