package br.com.modules.da.group.service.impl;

import java.util.HashSet;
import java.util.Set;

import org.springframework.stereotype.Service;

import br.com.modules.da.group.service.IGroupResourceLinkingService;
import br.com.modules.da.resource.dto.ResourceDTO;
import br.com.modules.da.resource.model.Resource;
import br.com.modules.da.resource.service.IResourceService;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class GroupResourceLinkingServiceImpl implements IGroupResourceLinkingService {
	
	@NonNull private final IResourceService resourceService;
	
	@Override
	public Set<Resource> vinculateResourceEntityList(Set<ResourceDTO> dtoList) {
		Set<Resource> resources = new HashSet<>();
		dtoList.forEach(dto -> 
		resources.add(resourceService.getOne(dto.getIdentifier().toString()).toEntity())
		);
		return resources;
	}

}
