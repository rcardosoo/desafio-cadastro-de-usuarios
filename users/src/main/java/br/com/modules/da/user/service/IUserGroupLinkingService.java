package br.com.modules.da.user.service;

import java.util.Set;

import br.com.modules.da.group.dto.GroupDTO;
import br.com.modules.da.group.model.Group;

public interface IUserGroupLinkingService {
	
	Set<Group> vinculateGroupEntityList(Set<GroupDTO> dtoList);
}
