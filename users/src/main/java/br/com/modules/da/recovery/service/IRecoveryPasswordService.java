package br.com.modules.da.recovery.service;

import br.com.modules.da.recovery.dto.RecoveryPasswordCredentialsDTO;

public interface IRecoveryPasswordService {
		
	void recoveryPassword(RecoveryPasswordCredentialsDTO recovery);
}
