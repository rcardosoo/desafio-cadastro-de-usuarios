package br.com.modules.da.mail.dto;

import java.util.List;
import java.util.Locale;

import org.thymeleaf.context.Context;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder(builderMethodName = "builder")
public class HTMLMailDTO {
	
	private String pagePath;
	
	private Context contextAttributes;
	
	private Locale locale;
	
	private List<String> to;
		
	private String subject;
}
