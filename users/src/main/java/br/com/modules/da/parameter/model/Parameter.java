package br.com.modules.da.parameter.model;

import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import br.com.modules.da.audit.model.Audit;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "TB_PARAMETER")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Parameter extends Audit {
	
    @Id
    @GeneratedValue
    @Column(columnDefinition = "BINARY(16)")
    private UUID identifier;
    
    private long jwtExpirationTimeInMillis;
    
    private boolean enableMultipleCompanies;
    
    
}
