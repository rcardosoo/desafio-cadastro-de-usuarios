package br.com.modules.da.user.model;

import javax.persistence.Column;
import javax.persistence.Embeddable;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Embeddable
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder(builderMethodName = "builder")
public class Contact {

    @Column(nullable = false, length = 11)
	private String primaryPhone;

	@Column(nullable = true, length = 11)
	private String secondaryPhone;

	private String ddd;

}
