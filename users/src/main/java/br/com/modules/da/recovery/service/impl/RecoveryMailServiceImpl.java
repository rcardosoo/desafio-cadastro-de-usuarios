package br.com.modules.da.recovery.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.thymeleaf.context.Context;

import br.com.modules.da.company.dto.CompanyDTO;
import br.com.modules.da.mail.dto.HTMLMailDTO;
import br.com.modules.da.mail.service.IMailService;
import br.com.modules.da.recovery.model.RecoveryPassword;
import br.com.modules.da.recovery.service.IRecoveryMailService;
import br.com.modules.da.user.dto.UserDTO;
import br.com.modules.da.util.Constants;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class RecoveryMailServiceImpl implements IRecoveryMailService {
	
	@NonNull private final IMailService mailService;
	
	@Value("${module.recovery-link.page-path}")
	private String secretKet;
	
	@Async
	@Override
	public void sendRecoveryMail(RecoveryPassword recovery, UserDTO user, CompanyDTO company, Locale locale) {
		Context ctx = this.setContextAttributes(recovery, user, company, locale);
		List<String> to = new ArrayList<>();
		to.add(user.getEmail());
		
		HTMLMailDTO mail = HTMLMailDTO.builder()
			.pagePath("html/recovery-password.html")
			.contextAttributes(ctx)
			.locale(locale)
			.subject(Constants.RECOVERY_EMAIL_TITLE)
			.to(to)
			.build();
		
		mailService.sendHTMLMail(mail);
	}

	private Context setContextAttributes(RecoveryPassword recovery, UserDTO user, CompanyDTO company, Locale locale) {
		final Context ctx = new Context(locale);
		ctx.setVariable("user", user);
		ctx.setVariable("company", company);
		ctx.setVariable("link", recovery.getLink());
		return ctx;
	}
}