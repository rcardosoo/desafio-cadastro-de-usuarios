package br.com.modules.da.user.service.impl;

import org.springframework.stereotype.Service;

import br.com.modules.da.exception.NotFoundException;
import br.com.modules.da.user.dto.UserDTO;
import br.com.modules.da.user.repository.IUserRepository;
import br.com.modules.da.user.service.IUserQueryService;
import br.com.modules.da.util.Constants;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class UserQueryServiceImpl implements IUserQueryService {

	@NonNull private final IUserRepository repository;

	@Override
	public UserDTO byEmail(String email) {
		return repository.findByEmail(email)
				.map(UserDTO::toDto)
				.orElseThrow(() -> 
				new NotFoundException(Constants.NOT_FOUND_USER));
	}

}
