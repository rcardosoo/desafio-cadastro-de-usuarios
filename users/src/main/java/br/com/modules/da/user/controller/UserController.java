package br.com.modules.da.user.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.modules.da.user.dto.UserDTO;
import br.com.modules.da.user.service.IUserService;
import br.com.modules.da.util.PageHeaders;

@RestController
@RequestMapping("/users")
public class UserController {
		
	private final IUserService service;

    public UserController(IUserService service) {
        this.service = service;
	}
    
    @PostMapping()
	public ResponseEntity<UserDTO> create(@Valid @RequestBody UserDTO user) {
		return ResponseEntity.ok(service.create(user));
	}
	
	@GetMapping("{id}")
	public ResponseEntity<UserDTO> getOne(@PathVariable String id) {
		return ResponseEntity.ok(service.getOne(id));
	}
	
	@GetMapping()
	public ResponseEntity<List<UserDTO>> getAll(Pageable pageable) {
		Page<UserDTO> list = service.getAll(pageable);
		return ResponseEntity.ok().headers(new PageHeaders(list)).body(list.getContent());
	}
	
	@PutMapping("{id}")
	public ResponseEntity<UserDTO> update(@PathVariable String id, @Valid @RequestBody UserDTO user) {
		return ResponseEntity.ok(service.update(id, user));
	}
	
	@DeleteMapping("{id}")
	public ResponseEntity<UserDTO> update(@PathVariable String id) {
		return ResponseEntity.ok(service.delete(id));
	}
}
