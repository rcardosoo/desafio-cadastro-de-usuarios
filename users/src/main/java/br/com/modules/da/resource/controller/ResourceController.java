package br.com.modules.da.resource.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.modules.da.resource.dto.ResourceDTO;
import br.com.modules.da.resource.service.IResourceService;
import br.com.modules.da.util.PageHeaders;

@RestController
@RequestMapping("/resources")
public class ResourceController {
		
	private final IResourceService service;

    public ResourceController(IResourceService service) {
        this.service = service;
	}
    
    @PostMapping()
	public ResponseEntity<ResourceDTO> create(@Valid @RequestBody ResourceDTO resource) {
		return ResponseEntity.ok(service.create(resource));
	}
	
	@GetMapping("{id}")
	public ResponseEntity<ResourceDTO> getOne(@PathVariable String id) {
		return ResponseEntity.ok(service.getOne(id));
	}
	
	@GetMapping()
	public ResponseEntity<List<ResourceDTO>> getAll(Pageable pageable) {
		Page<ResourceDTO> list = service.getAll(pageable);
		return ResponseEntity.ok().headers(new PageHeaders(list)).body(list.getContent());
	}
	
	@PutMapping("{id}")
	public ResponseEntity<ResourceDTO> update(@PathVariable String id, @Valid @RequestBody ResourceDTO resource) {
		return ResponseEntity.ok(service.update(id, resource));
	}
	
	@DeleteMapping("{id}")
	public ResponseEntity<ResourceDTO> update(@PathVariable String id) {
		return ResponseEntity.ok(service.delete(id));
	}
}
