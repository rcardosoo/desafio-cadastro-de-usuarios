package br.com.modules.da.user.service.impl;

import java.util.HashSet;
import java.util.Set;

import org.springframework.stereotype.Service;

import br.com.modules.da.group.dto.GroupDTO;
import br.com.modules.da.group.model.Group;
import br.com.modules.da.group.service.IGroupService;
import br.com.modules.da.user.service.IUserGroupLinkingService;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class UserGroupLinkingServiceImpl implements IUserGroupLinkingService {
	
	@NonNull private final IGroupService groupService;
	
	@Override
	public Set<Group> vinculateGroupEntityList(Set<GroupDTO> dtoList) {
		Set<Group> groups = new HashSet<>();
		dtoList.forEach(dto -> 
			groups.add(groupService.getOne(dto.getIdentifier().toString()).toEntity())
		);
		return groups;
	}

}
