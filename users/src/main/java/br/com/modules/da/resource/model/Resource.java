package br.com.modules.da.resource.model;

import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import br.com.modules.da.audit.model.Audit;
import br.com.modules.da.resource.enumeration.ResourceStatus;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "TB_RESOURCE")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder(builderMethodName = "builder")
public class Resource extends Audit {
	
    @Id
    @GeneratedValue
    @Column(columnDefinition = "BINARY(16)")
    private UUID identifier;
    
    @Column(length = 40, nullable = false)
    private String key;
	
    @Enumerated(EnumType.STRING)
    @Column(length = 20, nullable = false)
    private ResourceStatus status;
}
