package br.com.modules.da.parameter.service;

import br.com.modules.da.parameter.dto.ParameterDTO;

public interface IParameterService {
	
	ParameterDTO getParameters();
	
	ParameterDTO update(String id, ParameterDTO parameterDTO);
	
}
