package br.com.modules.da.group.dto;

import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

import javax.validation.constraints.NotBlank;

import br.com.modules.da.group.enumeration.GroupStatus;
import br.com.modules.da.group.model.Group;
import br.com.modules.da.mapper.MapperConverter;
import br.com.modules.da.resource.dto.ResourceDTO;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class GroupDTO {
	
    private UUID identifier;
    
    @NotBlank
    private String name;
    
    private GroupStatus status;
    
    private Set<ResourceDTO> resources;
    
    public static Set<GroupDTO> toDtoList(Set<Group> entities) {
    	Set<GroupDTO> dtoList = new HashSet<>();
    	entities.forEach(e -> dtoList.add(MapperConverter.convert(e, GroupDTO.class)));
    	return dtoList;
    }
    
    public static GroupDTO toDto(Group entity) {
    	return MapperConverter.convert(entity, GroupDTO.class);
    }
    
    public Group toEntity() {
    	return MapperConverter.convert(this, Group.class);
    }
}
