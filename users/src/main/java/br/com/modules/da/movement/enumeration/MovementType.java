package br.com.modules.da.movement.enumeration;

public enum MovementType {
	
	MANUAL_ADJUST("Lançamento manual para ajuste financeiro"), 
	INTERNAL_TRANSFER("Transferência interna de valores");
	
    private final String value;

    MovementType(String value) {
        this.value = value;
    }

	public String getValue() {
		return value;
	}
    
}
