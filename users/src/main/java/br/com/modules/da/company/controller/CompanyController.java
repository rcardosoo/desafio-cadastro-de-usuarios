package br.com.modules.da.company.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.modules.da.company.dto.CompanyDTO;
import br.com.modules.da.company.service.ICompanyService;
import br.com.modules.da.util.PageHeaders;

@RestController
@RequestMapping("/companies")
public class CompanyController {
		
	private final ICompanyService service;

    public CompanyController(ICompanyService service) {
        this.service = service;
	}
    
    @PostMapping()
	public ResponseEntity<CompanyDTO> create(@Valid @RequestBody CompanyDTO company) {
		return ResponseEntity.ok(service.create(company));
	}
	
	@GetMapping("{id}")
	public ResponseEntity<CompanyDTO> getOne(@PathVariable String id) {
		return ResponseEntity.ok(service.getOne(id));
	}
	
	@GetMapping()
	public ResponseEntity<List<CompanyDTO>> getAll(Pageable pageable) {
		Page<CompanyDTO> list = service.getAll(pageable);
		return ResponseEntity.ok().headers(new PageHeaders(list)).body(list.getContent());
	}
	
	@PutMapping("{id}")
	public ResponseEntity<CompanyDTO> update(@PathVariable String id, @Valid @RequestBody CompanyDTO company) {
		return ResponseEntity.ok(service.update(id, company));
	}
	
	@DeleteMapping("{id}")
	public ResponseEntity<CompanyDTO> update(@PathVariable String id) {
		return ResponseEntity.ok(service.delete(id));
	}
}