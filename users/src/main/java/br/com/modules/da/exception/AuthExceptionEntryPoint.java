package br.com.modules.da.exception;

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.ObjectMapper;

@Component
public class AuthExceptionEntryPoint implements AuthenticationEntryPoint {

	@Override
	public void commence(HttpServletRequest request, HttpServletResponse response,
			AuthenticationException authException) throws IOException, ServletException {
	}

	public void commence(HttpServletResponse response,
			Exception authException) throws IOException {
		final Map<String, Object> mapBodyException = new HashMap<>() ;

		mapBodyException.put("error", "Acesso negado") ;
		mapBodyException.put("message", authException.getMessage()) ;
		mapBodyException.put("timestamp", LocalDateTime.now().toString()) ;
		mapBodyException.put("status" , 401) ;

		response.setContentType("application/json") ;
		response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);

		final ObjectMapper mapper = new ObjectMapper() ;
		mapper.writeValue(response.getOutputStream(), mapBodyException);
	}

}