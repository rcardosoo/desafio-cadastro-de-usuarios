package br.com.modules.da.mail.service;

import br.com.modules.da.mail.dto.HTMLMailDTO;

public interface IMailService {
	
	void sendHTMLMail(HTMLMailDTO mail);
}
