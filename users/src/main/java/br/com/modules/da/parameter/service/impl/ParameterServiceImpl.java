package br.com.modules.da.parameter.service.impl;

import java.util.List;
import java.util.UUID;

import org.springframework.stereotype.Service;

import br.com.modules.da.exception.NotFoundException;
import br.com.modules.da.parameter.dto.ParameterDTO;
import br.com.modules.da.parameter.model.Parameter;
import br.com.modules.da.parameter.repository.IParameterRepository;
import br.com.modules.da.parameter.service.IParameterService;
import br.com.modules.da.util.Constants;
import br.com.modules.da.util.UUIDUtils;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class ParameterServiceImpl implements IParameterService {
	
	@NonNull private final IParameterRepository repository;

	@Override
	public ParameterDTO update(String id, ParameterDTO parameterDTO) {
		UUID identifier = UUIDUtils.validateString(id);

		return repository.findById(identifier).map(parameter -> {
			parameter.setJwtExpirationTimeInMillis(parameterDTO.getJwtExpirationTimeInMillis());
			parameter.setEnableMultipleCompanies(parameterDTO.isEnableMultipleCompanies());
			return ParameterDTO.toDto(repository.save(parameter));
		}).orElseThrow(() -> new NotFoundException(Constants.NOT_FOUND_PARAMETER));
	}

	@Override
	public ParameterDTO getParameters() {
		List<Parameter> parameters = repository.findAll();
		
		if (parameters.isEmpty()) {
			throw new NotFoundException(Constants.NOT_FOUND_PARAMETER);
		}
		
		return ParameterDTO.toDto(parameters.get(0));
	}

}
