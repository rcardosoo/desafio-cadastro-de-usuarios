package br.com.modules.da.mapper;

import org.modelmapper.ModelMapper;
import org.modelmapper.convention.MatchingStrategies;

public class MapperConverter {

	private static ModelMapper modelMapper = new ModelMapper();

    static {
        modelMapper = new ModelMapper();
        modelMapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);
    }

    private MapperConverter() {
    }

    public static <S, T> S convert(final T source, Class<S> targetClass) {
        return modelMapper.map(source, targetClass);
    }
   
}
