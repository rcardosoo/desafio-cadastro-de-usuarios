package br.com.modules.da.user.enumeration;

public enum UserStatus {
	
	ACTIVE,
	INACTIVE,
	BLOCKED;
}
