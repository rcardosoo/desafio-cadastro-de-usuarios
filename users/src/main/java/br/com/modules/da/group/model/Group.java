package br.com.modules.da.group.model;

import java.util.Set;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import br.com.modules.da.audit.model.Audit;
import br.com.modules.da.group.enumeration.GroupStatus;
import br.com.modules.da.resource.model.Resource;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "TB_GROUP")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Group extends Audit {
	
    @Id
    @GeneratedValue
    @Column(columnDefinition = "BINARY(16)")
    private UUID identifier;
    
    @Column(length = 80, nullable = false)
    private String name;
	
    @Enumerated(EnumType.STRING)
    @Column(length = 20, nullable = false)
    private GroupStatus status;
    
    @ManyToMany
    @JoinTable( name = "TB_GROUP_RESOURCE",
			    joinColumns = @JoinColumn(name = "group_identifier", referencedColumnName = "identifier"),
			    inverseJoinColumns = @JoinColumn(name = "resource_identifier", referencedColumnName = "identifier"))
    private Set<Resource> resources;
}
