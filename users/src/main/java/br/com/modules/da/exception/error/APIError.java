package br.com.modules.da.exception.error;

import java.time.LocalDateTime;
import java.util.List;

public class APIError {

	private String error;

	private List<String> message;

	private LocalDateTime timestamp;

	private int status;

	public APIError(String error, List<String> message, int status) {
		this.error = error;
		this.message = message;
		this.status = status;
		this.timestamp = LocalDateTime.now();
	}

	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}

	public List<String> getMessage() {
		return message;
	}

	public void setMessage(List<String> message) {
		this.message = message;
	}

	public LocalDateTime getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(LocalDateTime timestamp) {
		this.timestamp = timestamp;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

}
