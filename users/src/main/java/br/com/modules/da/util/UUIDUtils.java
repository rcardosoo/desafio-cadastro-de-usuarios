package br.com.modules.da.util;

import java.util.UUID;

import br.com.modules.da.exception.BadRequestException;

public class UUIDUtils {

	private UUIDUtils() {
		throw new IllegalStateException();
	}

	public static UUID validateString(String identifier) {
		try {
			return UUID.fromString(identifier);
		} catch(Exception e) {
			throw new BadRequestException(Constants.INVALID_IDENTIFIER);
		}
	}
}
