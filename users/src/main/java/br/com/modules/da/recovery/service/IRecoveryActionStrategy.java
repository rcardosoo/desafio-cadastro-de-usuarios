package br.com.modules.da.recovery.service;

import br.com.modules.da.recovery.model.RecoveryPassword;

public interface IRecoveryActionStrategy {
	
	void recoveryAction(RecoveryPassword recovery, String password);
}
