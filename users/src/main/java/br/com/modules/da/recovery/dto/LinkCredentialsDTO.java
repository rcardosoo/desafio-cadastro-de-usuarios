package br.com.modules.da.recovery.dto;

import javax.validation.constraints.NotBlank;

import lombok.Getter;
import lombok.Setter;

public class LinkCredentialsDTO {
	
	@Getter
	@Setter
	@NotBlank
	private String email;	
	
}
