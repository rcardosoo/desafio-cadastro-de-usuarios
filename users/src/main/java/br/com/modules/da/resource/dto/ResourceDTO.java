package br.com.modules.da.resource.dto;

import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

import br.com.modules.da.mapper.MapperConverter;
import br.com.modules.da.resource.enumeration.ResourceStatus;
import br.com.modules.da.resource.model.Resource;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class ResourceDTO {

	private UUID identifier;

	private String key;

	private ResourceStatus status;
	
    public static Set<ResourceDTO> toDtoList(Set<Resource> entities) {
    	Set<ResourceDTO> dtoList = new HashSet<>();
    	entities.forEach(e -> dtoList.add(MapperConverter.convert(e, ResourceDTO.class)));
    	return dtoList;
    }
    
    public static ResourceDTO toDto(Resource entity) {
    	return MapperConverter.convert(entity, ResourceDTO.class);
    }
    
    public Resource toEntity() {
    	return MapperConverter.convert(this, Resource.class);
    }
}
