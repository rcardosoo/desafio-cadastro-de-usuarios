package br.com.modules.da.parameter.dto;

import java.util.UUID;

import br.com.modules.da.mapper.MapperConverter;
import br.com.modules.da.parameter.model.Parameter;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class ParameterDTO {
	
    private UUID identifier;
    
    private long jwtExpirationTimeInMillis;
    
    private boolean enableMultipleCompanies;
        
    public static ParameterDTO toDto(Parameter entity) {
    	return MapperConverter.convert(entity, ParameterDTO.class);
    }
    
    public Parameter toEntity() {
    	return MapperConverter.convert(this, Parameter.class);
    }
    
}
