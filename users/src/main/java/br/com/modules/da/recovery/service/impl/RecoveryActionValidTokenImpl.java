package br.com.modules.da.recovery.service.impl;

import java.util.UUID;

import org.springframework.stereotype.Service;

import br.com.modules.da.recovery.enumeration.RecoveryPasswordStatus;
import br.com.modules.da.recovery.model.RecoveryPassword;
import br.com.modules.da.recovery.repository.IRecoveryPasswordRepository;
import br.com.modules.da.recovery.service.IRecoveryActionStrategy;
import br.com.modules.da.user.dto.UserDTO;
import br.com.modules.da.user.service.IUserService;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Service("recoveryActionValid")
public class RecoveryActionValidTokenImpl implements IRecoveryActionStrategy {
	
	@NonNull private final IRecoveryPasswordRepository repository;
	@NonNull private final IUserService userService;

	@Override
	public void recoveryAction(RecoveryPassword recovery, String password) {
		this.updateUserPassword(recovery.getUserIdentifier(), password);
		recovery.setStatus(RecoveryPasswordStatus.CHANGED);
		repository.save(recovery);
	}

	private void updateUserPassword(UUID userIdentifier, String password) {
		UserDTO user = userService.getOne(userIdentifier.toString());
		user.setPassword(password);
		userService.update(userIdentifier.toString(), user);
	}

}
