package br.com.modules.da.user.service;

import br.com.modules.da.user.dto.UserDTO;

public interface IUserQueryService {
	
	UserDTO byEmail(String email);
}
