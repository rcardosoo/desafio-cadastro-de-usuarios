package br.com.modules.da.user.dto;

import java.util.Set;
import java.util.UUID;

import javax.validation.Valid;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.br.CPF;

import br.com.modules.da.company.dto.CompanyDTO;
import br.com.modules.da.group.dto.GroupDTO;
import br.com.modules.da.mapper.MapperConverter;
import br.com.modules.da.resource.dto.ResourceDTO;
import br.com.modules.da.user.enumeration.UserStatus;
import br.com.modules.da.user.model.User;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class UserDTO {
	
	private UUID identifier;
    
    @NotBlank
    private String name;
    
    @NotBlank
    @CPF
    private String cpf;
    
	@Email
	@Length(max=60)
	private String email;
    
    @Valid
	private AddressDTO address;
    
    @Valid
    private ContactDTO contact;
    
    private String password;
    
    private UserStatus status;
    
    private CompanyDTO company;
    
    private Set<GroupDTO> groups;
    
    private Set<ResourceDTO> resources;
    
    public static UserDTO toDto(User entity) {
    	return MapperConverter.convert(entity, UserDTO.class);
    }
    
    public User toEntity() {
    	return MapperConverter.convert(this, User.class);
    }

}