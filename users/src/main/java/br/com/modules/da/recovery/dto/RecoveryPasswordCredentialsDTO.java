package br.com.modules.da.recovery.dto;

import javax.validation.constraints.NotBlank;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RecoveryPasswordCredentialsDTO {
	
	@NotBlank
	private String password;
	
	@NotBlank
	private String token;
	
}
