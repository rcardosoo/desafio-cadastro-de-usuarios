package br.com.modules.da.company.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import br.com.modules.da.company.dto.CompanyDTO;
import br.com.modules.da.company.model.Company;
import br.com.modules.da.company.repository.ICompanyRepository;
import br.com.modules.da.company.service.ICompanyService;
import br.com.modules.da.exception.NotFoundException;
import br.com.modules.da.util.Constants;
import br.com.modules.da.util.UUIDUtils;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class CompanyServiceImpl implements ICompanyService {
	
	@NonNull private final ICompanyRepository repository;

	@Override
	public CompanyDTO create(CompanyDTO companyDTO) {
		Company company = companyDTO.toEntity();
		return CompanyDTO.toDto(repository.save(company));
	}

	@Override
	public CompanyDTO getOne(String id) {
		UUID identifier = UUIDUtils.validateString(id);

		return repository.findById(identifier).map(CompanyDTO::toDto)
				.orElseThrow(() -> new NotFoundException(Constants.NOT_FOUND_COMPANY));
	}

	@Override
	public CompanyDTO update(String id, CompanyDTO companyDTO) {
		UUID identifier = UUIDUtils.validateString(id);

		return repository.findById(identifier).map(company -> {
			company.setName(companyDTO.getName());
			company.setStatus(companyDTO.getStatus());
			return CompanyDTO.toDto(repository.save(company));
		}).orElseThrow(() -> new NotFoundException(Constants.NOT_FOUND_COMPANY));
	}

	@Override
	public CompanyDTO delete(String id) {
		UUID identifier = UUIDUtils.validateString(id);
		
		return repository.findById(identifier).map(company -> {
			repository.deleteById(identifier);
			return CompanyDTO.toDto(company);
        }).orElseThrow(() -> new NotFoundException(Constants.NOT_FOUND_COMPANY));
	}

	@Override
	public Page<CompanyDTO> getAll(Pageable pageable) {
        List<CompanyDTO> dtoList = new ArrayList<>();
		Page<Company> pageResult = repository.findAll(pageable);
		pageResult.forEach(resource -> dtoList.add(CompanyDTO.toDto(resource)));
		return new PageImpl<>(dtoList, pageable, pageResult.getTotalElements());
	}

}
