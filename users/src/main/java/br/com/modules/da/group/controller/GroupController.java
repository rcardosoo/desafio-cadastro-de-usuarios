package br.com.modules.da.group.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.modules.da.group.dto.GroupDTO;
import br.com.modules.da.group.service.IGroupService;
import br.com.modules.da.util.PageHeaders;

@RestController
@RequestMapping("/groups")
public class GroupController {
		
	private final IGroupService service;

    public GroupController(IGroupService service) {
        this.service = service;
	}
    
    @PostMapping()
	public ResponseEntity<GroupDTO> create(@Valid @RequestBody GroupDTO group) {
		return ResponseEntity.ok(service.create(group));
	}
	
	@GetMapping("{id}")
	public ResponseEntity<GroupDTO> getOne(@PathVariable String id) {
		return ResponseEntity.ok(service.getOne(id));
	}
	
	@GetMapping()
	public ResponseEntity<List<GroupDTO>> getAll(Pageable pageable) {
		Page<GroupDTO> list = service.getAll(pageable);
		return ResponseEntity.ok().headers(new PageHeaders(list)).body(list.getContent());
	}
	
	@PutMapping("{id}")
	public ResponseEntity<GroupDTO> update(@PathVariable String id, @Valid @RequestBody GroupDTO group) {
		return ResponseEntity.ok(service.update(id, group));
	}
	
	@DeleteMapping("{id}")
	public ResponseEntity<GroupDTO> update(@PathVariable String id) {
		return ResponseEntity.ok(service.delete(id));
	}
}
