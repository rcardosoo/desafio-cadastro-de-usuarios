package br.com.modules.da.group.service;

import java.util.Set;

import br.com.modules.da.resource.dto.ResourceDTO;
import br.com.modules.da.resource.model.Resource;

public interface IGroupResourceLinkingService {
	
	Set<Resource> vinculateResourceEntityList(Set<ResourceDTO> dtoList);
}
