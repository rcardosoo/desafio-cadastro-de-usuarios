package br.com.modules.da.resource.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import br.com.modules.da.exception.NotFoundException;
import br.com.modules.da.resource.dto.ResourceDTO;
import br.com.modules.da.resource.model.Resource;
import br.com.modules.da.resource.repository.IResourceRepository;
import br.com.modules.da.resource.service.IResourceService;
import br.com.modules.da.util.Constants;
import br.com.modules.da.util.UUIDUtils;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class ResourceServiceImpl implements IResourceService {
	
	@NonNull private final IResourceRepository repository;

	@Override
	public ResourceDTO create(ResourceDTO resourceDTO) {
		Resource resource = resourceDTO.toEntity();
		return ResourceDTO.toDto(repository.save(resource));
	}

	@Override
	public ResourceDTO getOne(String id) {
		UUID identifier = UUIDUtils.validateString(id);

		return repository.findById(identifier).map(ResourceDTO::toDto)
				.orElseThrow(() -> new NotFoundException(Constants.NOT_FOUND_RESOURCE));
	}

	@Override
	public ResourceDTO update(String id, ResourceDTO resourceDTO) {
		UUID identifier = UUIDUtils.validateString(id);

		return repository.findById(identifier).map(resource -> {
			resource.setKey(resourceDTO.getKey());
			resource.setStatus(resourceDTO.getStatus());
			return ResourceDTO.toDto(repository.save(resource));
		}).orElseThrow(() -> new NotFoundException(Constants.NOT_FOUND_RESOURCE));
	}

	@Override
	public ResourceDTO delete(String id) {
		UUID identifier = UUIDUtils.validateString(id);
		
		return repository.findById(identifier).map(resource -> {
			repository.deleteById(identifier);
			return ResourceDTO.toDto(resource);
        }).orElseThrow(() -> new NotFoundException(Constants.NOT_FOUND_RESOURCE));
	}

	@Override
	public Page<ResourceDTO> getAll(Pageable pageable) {
        List<ResourceDTO> dtoList = new ArrayList<>();
		Page<Resource> pageResult = repository.findAll(pageable);
		pageResult.forEach(resource -> dtoList.add(ResourceDTO.toDto(resource)));
		return new PageImpl<>(dtoList, pageable, pageResult.getTotalElements());
	}

}
