package br.com.modules.da.account.model;

import java.math.BigDecimal;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Digits;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "TB_ACCOUNT")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Account {
	
    @Id
    @GeneratedValue
    @Column(columnDefinition = "BINARY(16)")
    private UUID identifier;
    	
    @Digits(integer=10, fraction=2)
    @Column(nullable = false)
    private BigDecimal availableBalance = BigDecimal.ZERO;
    
    @Digits(integer=10, fraction=2)
    @Column(nullable = false)
    private BigDecimal blockedBalance = BigDecimal.ZERO;
}
