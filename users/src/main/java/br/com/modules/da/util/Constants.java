package br.com.modules.da.util;

public class Constants {
	
	private Constants() {
		throw new IllegalStateException();
	}

	public static final String NOT_FOUND_EXCEPTION_TITLE = "Recurso não encontrado";
	public static final String CONFLICT_EXCEPTION_TITLE = "Recurso já existente";
	public static final String BAD_REQUEST_EXCEPTION_TITLE = "Parâmetros incorretos";
	public static final String NOT_FOUND_USER = "Usuário não econtrado";
	public static final String NOT_FOUND_GROUP = "Grupo não econtrado";
	public static final String NOT_FOUND_COMPANY = "Companhia não econtrada";
	public static final String NOT_FOUND_PARAMETER = "Registro de parâmetros não econtrado";
	public static final String COMPANY_NOT_FOUND = "ID da companhia inválido";
	public static final String USER_CONFLICT = "Usuário já cadastrado na companhia";
	public static final String NOT_FOUND_RESOURCE = "Resource não encontrato";
	public static final String HEADER_STRING_AUTHORIZATION = "Authorization";
	public static final String JWT_BEARER_TOKEN_PREFIX = "Bearer";
	public static final String EXPIRED_TOKEN = "Token de acesso expirado";
	public static final String INVALID_TOKEN = "Token de acesso inválido";
	public static final String INVALID_IDENTIFIER = "Identificador inválido";
	public static final String UNAUTHORIZED_EXCEPTION_TITLE = "Acesso negado";
	public static final String INVALID_CREDENTIALS = "Crendenciais de acesso incorretas";
	public static final String RECOVERY_EMAIL_CONFLICT = "Email para recuperação de senha já enviado";
	public static final String RECOVERY_EMAIL_TITLE = "Recuperação de senha";
	public static final String INVALID_RECOVERY_TOKEN = "Token de recuperação inválido";
	public static final String EXPIRED_RECOVERY_TOKEN = "Token de recuperação expirado";
	public static final String EXPIRED_RECOVERY_LINK = "Link para recuperação de senha expirado";
	public static final String USED_RECOVERY_LINK = "Link para recuperação de senha já utilizado";
}
