package br.com.modules.da.movement.enumeration;

public enum EntryType {
	
	CREDIT, DEBIT;
}
