package br.com.modules.da.user.dto;

import javax.validation.constraints.NotBlank;

import br.com.modules.da.mapper.MapperConverter;
import br.com.modules.da.user.model.Contact;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class ContactDTO {
	
    @NotBlank
	private String primaryPhone;

	private String secondaryPhone;

	private String ddd;
	
    public static ContactDTO toDto(Contact entity) {
    	return MapperConverter.convert(entity, ContactDTO.class);
    }
    
    public Contact toEntity() {
    	return MapperConverter.convert(this, Contact.class);
    }
}
