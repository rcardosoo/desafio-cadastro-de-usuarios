package br.com.modules.da.auth.service.impl;

import java.util.concurrent.TimeUnit;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import br.com.modules.da.auth.dto.AuthCredentialsDTO;
import br.com.modules.da.auth.dto.AuthResponseDTO;
import br.com.modules.da.auth.service.IAuthService;
import br.com.modules.da.exception.UnauthorizedException;
import br.com.modules.da.parameter.dto.ParameterDTO;
import br.com.modules.da.parameter.service.IParameterService;
import br.com.modules.da.security.service.ISecurityService;
import br.com.modules.da.user.repository.IUserRepository;
import br.com.modules.da.util.Constants;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
@RequiredArgsConstructor
public class AuthServiceImpl implements IAuthService {
	
	@NonNull private final IUserRepository userRepository;
	@NonNull private final BCryptPasswordEncoder encoder;
	@NonNull private final ISecurityService securityService;
	@NonNull private final IParameterService parameterService;
	
	@Value("${module.security.secret-key}")
	private String secretKet;
	
	@Override
	public AuthResponseDTO authenticate(AuthCredentialsDTO credentials, HttpServletResponse response) {
		return userRepository.findByEmail(credentials.getEmail()).map(user -> {
			
			log.info("Login attempt - email: " + credentials.getEmail());
			
			if (encoder.matches(credentials.getPassword(), user.getPassword())) {
				ParameterDTO parameters = parameterService.getParameters();
				long expTime = parameters.getJwtExpirationTimeInMillis();
				
				String token = securityService
						.generateAuthorizationToken(user.getIdentifier().toString(), secretKet, expTime);
				
				response.addHeader("ACCESS_TOKEN", token);
				response.addHeader("EXPIRES_IN_SECONDS", TimeUnit.MILLISECONDS.toSeconds(expTime) + "");
				return AuthResponseDTO.toDto(user);
			}
			
			log.info("Login failed - incorrect password");
			throw new UnauthorizedException(Constants.INVALID_CREDENTIALS);
		}).orElseThrow(() -> new UnauthorizedException(Constants.INVALID_CREDENTIALS));
	}

}
