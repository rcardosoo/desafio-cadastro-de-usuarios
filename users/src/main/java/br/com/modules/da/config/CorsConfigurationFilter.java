package br.com.modules.da.config;

import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.web.filter.CorsFilter;

@Configuration
@SuppressWarnings({"rawtypes", "unchecked"})
public class CorsConfigurationFilter {

	@Bean
	public FilterRegistrationBean corsFilter() {
		UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
		CorsConfiguration config = new CorsConfiguration().applyPermitDefaultValues();
		config.addAllowedMethod(HttpMethod.PUT);
		config.addAllowedMethod(HttpMethod.DELETE);
		
		config.addExposedHeader("Page");
		config.addExposedHeader("ElementsPerPage");
		config.addExposedHeader("TotalPages");
		config.addExposedHeader("TotalElements");
		config.addExposedHeader("TotalElementsOnPage");
		config.addExposedHeader("ACCESS_TOKEN");
		config.addExposedHeader("EXPIRES_IN_SECONDS");

		source.registerCorsConfiguration("/**", config);
		FilterRegistrationBean bean = new FilterRegistrationBean(new CorsFilter(source));
		bean.setOrder(1);
		return bean;
	}
}