package br.com.modules.da.movement.model;

import java.math.BigDecimal;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Digits;

import br.com.modules.da.movement.enumeration.EntryType;
import br.com.modules.da.movement.enumeration.MovementStatus;
import br.com.modules.da.movement.enumeration.MovementType;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "TB_MOVEMENT")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder(builderMethodName = "builder")
public class Movement {
	
    @Id
    @GeneratedValue
    @Column(columnDefinition = "BINARY(16)")
    private UUID identifier;
    
    @Enumerated(EnumType.STRING)
    @Column(length = 20, nullable = false)
    private EntryType entryType;
    
    @Enumerated(EnumType.STRING)
    @Column(length = 100, nullable = false)
    private MovementType movementType;
    
    @Column(nullable = false)
    @Digits(integer=10, fraction=2)
    private BigDecimal value;
    
    @Column(length = 100, nullable = true)
    private String description;
    
    @Enumerated(EnumType.STRING)
    @Column(length = 20, nullable = false)
    private MovementStatus status;
}
