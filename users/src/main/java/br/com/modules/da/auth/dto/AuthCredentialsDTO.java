package br.com.modules.da.auth.dto;

import javax.validation.constraints.NotBlank;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AuthCredentialsDTO {
	
	@NotBlank
	private String email;
	
	@NotBlank
	private String password;
}
