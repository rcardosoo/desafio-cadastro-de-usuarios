package br.com.modules.da.user.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import br.com.modules.da.user.dto.UserDTO;

public interface IUserService {
	
	UserDTO create(UserDTO userDTO);
	
	UserDTO getOne(String id);
	
	Page<UserDTO> getAll(Pageable pageable);
	
	UserDTO update(String id, UserDTO userDTO);
	
	UserDTO delete(String id);
	
}
