package br.com.modules.da.recovery.repository;

import java.util.Optional;
import java.util.UUID;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import br.com.modules.da.recovery.model.RecoveryPassword;

@Repository
public interface IRecoveryPasswordRepository extends PagingAndSortingRepository<RecoveryPassword, UUID> {
	
    Optional<RecoveryPassword> findByToken(String token);
	
	Optional<RecoveryPassword> findByUserIdentifierAndStatus(UUID useridentifier, String status);
}
