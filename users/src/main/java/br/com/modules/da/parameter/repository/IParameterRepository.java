package br.com.modules.da.parameter.repository;

import java.util.UUID;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.modules.da.parameter.model.Parameter;

@Repository
public interface IParameterRepository extends JpaRepository<Parameter, UUID> {
	
}
