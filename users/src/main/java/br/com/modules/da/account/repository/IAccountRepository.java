package br.com.modules.da.account.repository;

import java.util.UUID;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.modules.da.account.model.Account;

@Repository
public interface IAccountRepository extends JpaRepository<Account, UUID> {
	
}

