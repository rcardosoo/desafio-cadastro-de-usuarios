package br.com.modules.da.exception.handler;

import java.util.ArrayList;
import java.util.List;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import br.com.modules.da.exception.BadRequestException;
import br.com.modules.da.exception.ConflictException;
import br.com.modules.da.exception.NotFoundException;
import br.com.modules.da.exception.UnauthorizedException;
import br.com.modules.da.exception.error.APIError;
import br.com.modules.da.util.Constants;

@ControllerAdvice(annotations = RestController.class)
public class ControllerExceptionHandler extends ResponseEntityExceptionHandler {

	@ExceptionHandler(NotFoundException.class)
	public ResponseEntity<APIError> notFoundException(final NotFoundException e) {
		List<String> errorMessage = new ArrayList<>();
		errorMessage.add(e.getMessage());
		APIError err = new APIError(Constants.NOT_FOUND_EXCEPTION_TITLE, errorMessage, HttpStatus.NOT_FOUND.value());
		return new ResponseEntity<>(err, HttpStatus.NOT_FOUND);
	}

	@ExceptionHandler({ BadRequestException.class, IllegalArgumentException.class })
	public ResponseEntity<APIError> badRequestException(final BadRequestException e) {
		List<String> errorMessage = new ArrayList<>();
		errorMessage.add(e.getMessage());
		APIError err = new APIError(Constants.BAD_REQUEST_EXCEPTION_TITLE, errorMessage,
				HttpStatus.BAD_REQUEST.value());
		return new ResponseEntity<>(err, HttpStatus.BAD_REQUEST);
	}

	@ExceptionHandler(ConflictException.class)
	public ResponseEntity<APIError> conflictException(final ConflictException e) {
		List<String> errorMessage = new ArrayList<>();
		errorMessage.add(e.getMessage());
		APIError err = new APIError(Constants.CONFLICT_EXCEPTION_TITLE, errorMessage, HttpStatus.CONFLICT.value());
		return new ResponseEntity<>(err, HttpStatus.CONFLICT);
	}
	
	@ExceptionHandler(UnauthorizedException.class)
	public ResponseEntity<APIError> unauthorizedException(final UnauthorizedException e) {
		List<String> errorMessage = new ArrayList<>();
		errorMessage.add(e.getMessage());
		APIError err = new APIError(Constants.UNAUTHORIZED_EXCEPTION_TITLE, errorMessage, HttpStatus.UNAUTHORIZED.value());
		return new ResponseEntity<>(err, HttpStatus.UNAUTHORIZED);
	}

	@Override
	protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex,
			HttpHeaders headers, HttpStatus status, WebRequest request) {
		List<String> details = new ArrayList<>();
		for (ObjectError error : ex.getBindingResult().getAllErrors()) {
			details.add(((FieldError) error).getField() + " - " + error.getDefaultMessage());
		}
		APIError err = new APIError(Constants.BAD_REQUEST_EXCEPTION_TITLE, details, HttpStatus.BAD_REQUEST.value());
		return new ResponseEntity<>(err, HttpStatus.BAD_REQUEST);
	}
}
