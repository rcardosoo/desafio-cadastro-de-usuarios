package br.com.modules.da.company.enumeration;

public enum CompanyStatus {
	
	ACTIVE,
	INACTIVE;
}
