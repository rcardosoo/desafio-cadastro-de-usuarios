package br.com.modules.da.recovery.service.impl;

import java.time.Duration;
import java.time.LocalDateTime;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import br.com.modules.da.exception.BadRequestException;
import br.com.modules.da.recovery.dto.RecoveryPasswordCredentialsDTO;
import br.com.modules.da.recovery.enumeration.RecoveryPasswordStatus;
import br.com.modules.da.recovery.model.RecoveryPassword;
import br.com.modules.da.recovery.repository.IRecoveryPasswordRepository;
import br.com.modules.da.recovery.service.IRecoveryActionStrategy;
import br.com.modules.da.recovery.service.IRecoveryPasswordService;
import br.com.modules.da.util.Constants;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class RecoveryPasswordServiceImpl implements IRecoveryPasswordService {
	
	@NonNull private final IRecoveryPasswordRepository repository;
	@NonNull @Qualifier("recoveryActionExpired") private final IRecoveryActionStrategy actionExpiredService;
	@NonNull @Qualifier("recoveryActionValid") private final IRecoveryActionStrategy actionValidService;

	@Value("${module.recovery.expiration-time")
	private String expTime;
	
	@Override
	public void recoveryPassword(RecoveryPasswordCredentialsDTO recoveryDTO) {
		RecoveryPassword recovery = this.getRecoveryFromToken(recoveryDTO.getToken());
		this.validateRecoveryStatus(recovery.getStatus());
		IRecoveryActionStrategy recoveryAction = this.recoveryActionFactory(recovery);
		recoveryAction.recoveryAction(recovery, recoveryDTO.getPassword());
	}
	
	private IRecoveryActionStrategy recoveryActionFactory(RecoveryPassword recovery) {
		Duration duration = Duration.between(recovery.getCreatedAt(), LocalDateTime.now());
		if (duration.toMillis() < Long.valueOf(expTime)) {
			return actionValidService;
		}
		
		return actionExpiredService;
	}
	
	private void validateRecoveryStatus(RecoveryPasswordStatus status) {
		if (status.equals(RecoveryPasswordStatus.EXPIRED)) {
			throw new BadRequestException(Constants.EXPIRED_RECOVERY_LINK);
		}
		
		if (status.equals(RecoveryPasswordStatus.CHANGED)) {
			throw new BadRequestException(Constants.USED_RECOVERY_LINK);
		}
	}
	
	private RecoveryPassword getRecoveryFromToken(String token) {
		return repository.findByToken(token)
				.orElseThrow(() -> 
				new BadRequestException(Constants.INVALID_RECOVERY_TOKEN));
	}

}
