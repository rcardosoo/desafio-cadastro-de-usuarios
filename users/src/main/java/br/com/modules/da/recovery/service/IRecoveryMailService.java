package br.com.modules.da.recovery.service;

import java.util.Locale;

import br.com.modules.da.company.dto.CompanyDTO;
import br.com.modules.da.recovery.model.RecoveryPassword;
import br.com.modules.da.user.dto.UserDTO;

public interface IRecoveryMailService {
	
	void sendRecoveryMail(RecoveryPassword recovery, UserDTO user, CompanyDTO company, Locale locale);
}
