package br.com.modules.da.parameter.controller;

import javax.validation.Valid;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.modules.da.parameter.dto.ParameterDTO;
import br.com.modules.da.parameter.service.IParameterService;

@RestController
@RequestMapping("/parameters")
public class ParameterController {
	
	private final IParameterService service;

	public ParameterController(IParameterService service) {
		this.service = service;
	}
	
    @PutMapping("{id}")
	public ResponseEntity<ParameterDTO> update(@PathVariable String id, 
			@Valid @RequestBody ParameterDTO parameterDTO) {
		return ResponseEntity.ok(service.update(id, parameterDTO));
	}
    
	@GetMapping()
	public ResponseEntity<ParameterDTO> getParameters() {
		return ResponseEntity.ok(service.getParameters());
	}
}
