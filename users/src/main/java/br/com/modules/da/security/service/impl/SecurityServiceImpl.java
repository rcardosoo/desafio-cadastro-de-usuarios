package br.com.modules.da.security.service.impl;

import java.util.Date;

import org.springframework.stereotype.Service;

import br.com.modules.da.security.service.ISecurityService;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

@Service
public class SecurityServiceImpl implements ISecurityService {

	@Override
	public Claims decryptAuthorizationToken(String secretKey, String token) {
		return Jwts.parser()
				.setSigningKey(secretKey)
				.parseClaimsJws(token)
				.getBody();
	}

	@Override
	public String generateAuthorizationToken(String subject, String secret, long expirationTime) {
		return Jwts.builder()
				.setExpiration(new Date(System.currentTimeMillis() + expirationTime))
				.signWith(SignatureAlgorithm.HS256, secret)
				.compact();
	}
	
}
