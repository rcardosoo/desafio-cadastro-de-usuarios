package br.com.modules.da.user.dto;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import br.com.modules.da.mapper.MapperConverter;
import br.com.modules.da.user.model.Address;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class AddressDTO {

    @NotBlank
	@Size(min=1, max = 10)
	private String numberHome;

	@NotBlank
	@Size(min=3, max = 100)
	private String publicNameStreet;

	@NotBlank
	@Size(min=3, max = 50)
	private String district;

	@NotBlank
	@Size(min=3, max = 40)
	private String city;

	@NotBlank
	private String postalCode;

	@NotBlank
	private String state;

	@Size(max = 100)
    private String complement;

	private String landmark;

    public static AddressDTO toDto(Address entity) {
    	return MapperConverter.convert(entity, AddressDTO.class);
    }
    
    public Address toEntity() {
    	return MapperConverter.convert(this, Address.class);
    }
}
