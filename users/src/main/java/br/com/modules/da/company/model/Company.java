package br.com.modules.da.company.model;

import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import br.com.modules.da.audit.model.Audit;
import br.com.modules.da.company.enumeration.CompanyStatus;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "TB_COMPANY")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Company extends Audit {
	
    @Id
    @GeneratedValue
    @Column(columnDefinition = "BINARY(16)")
    private UUID identifier;
    
    @Column(length = 80, nullable = false)
    private String name;
	
    @Enumerated(EnumType.STRING)
    @Column(length = 20, nullable = false)
    private CompanyStatus status;
   
    @Column(nullable = true)
    private String clientURL;
    
    @Column(nullable = true)
    private String notificationEmail;
}
