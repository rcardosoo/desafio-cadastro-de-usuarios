package br.com.modules.da.group.enumeration;

public enum GroupStatus {
	
	ACTIVE,
	INACTIVE;
}
