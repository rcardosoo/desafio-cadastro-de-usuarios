package br.com.modules.da.user.model;

import java.util.Set;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import br.com.modules.da.account.model.Account;
import br.com.modules.da.audit.model.Audit;
import br.com.modules.da.company.model.Company;
import br.com.modules.da.group.model.Group;
import br.com.modules.da.resource.model.Resource;
import br.com.modules.da.user.enumeration.UserStatus;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder(builderMethodName = "builder")
@Table(name = "TB_USER",
	uniqueConstraints = { 
		@UniqueConstraint(columnNames = "cpf"),
		@UniqueConstraint(columnNames = "email") 
	})
public class User extends Audit {
	
    @Id
    @GeneratedValue
    @Column(columnDefinition = "BINARY(16)")
    private UUID identifier;
    
    @Column(length = 80, nullable = false)
    private String name;

    @Column(length = 11, name="cpf", nullable = false)
    private String cpf;
    
    @Column(length = 100, name="email", nullable = false)
	private String email;
    
    @Column(nullable = false)
    private String password;

    @Embedded
	private Address address;
	
	@Embedded
    private Contact contact;
	
    @Enumerated(EnumType.STRING)
    @Column(length = 20, nullable = false)
	private UserStatus status;
    
    @OneToOne
    private Account account;
    
    @ManyToOne
    private Company company;
	
    @ManyToMany(fetch = FetchType.LAZY)    
    @JoinTable( name = "TB_USER_GROUP",
			    joinColumns = @JoinColumn(name = "users_identifier", referencedColumnName = "identifier"),
			    inverseJoinColumns = @JoinColumn(name = "group_identifier", referencedColumnName = "identifier"))
    private Set<Group> groups;
    
    @ManyToMany
    @JoinTable( name = "TB_USER_RESOURCE",
			    joinColumns = @JoinColumn(name = "user_identifier", referencedColumnName = "identifier"),
			    inverseJoinColumns = @JoinColumn(name = "resource_identifier", referencedColumnName = "identifier"))
    private Set<Resource> resources;

}
