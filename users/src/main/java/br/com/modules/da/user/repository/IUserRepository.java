package br.com.modules.da.user.repository;

import java.util.Optional;
import java.util.UUID;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.modules.da.user.model.User;

@Repository
public interface IUserRepository extends JpaRepository<User, UUID> {
		
	Optional<User> findByEmail(String email);
}
