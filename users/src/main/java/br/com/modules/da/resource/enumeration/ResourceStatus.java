package br.com.modules.da.resource.enumeration;

public enum ResourceStatus {
	
	ENABLED,
	DISABLED;
}
