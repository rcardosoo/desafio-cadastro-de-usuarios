package br.com.modules.da.user.model;

import javax.persistence.Column;
import javax.persistence.Embeddable;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Embeddable
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder(builderMethodName = "builder")
public class Address {

    @Column(nullable = false, length = 10)
	private String numberHome;

	@Column(nullable = false, length = 100)
	private String publicNameStreet;

	@Column(nullable = false, length = 50)
	private String district;

	@Column(nullable = false, length = 40)
	private String city;

	@Column(nullable = false, length = 8)
	private String postalCode;

	@Column(nullable = false, length = 2)
	private String state;

	@Column(length = 100)
    private String complement;
	
	@Column(length = 255, nullable = true)
    private String landmark;
}
