package br.com.modules.da.auth.service;

import javax.servlet.http.HttpServletResponse;

import br.com.modules.da.auth.dto.AuthCredentialsDTO;
import br.com.modules.da.auth.dto.AuthResponseDTO;

public interface IAuthService {
	
	AuthResponseDTO authenticate(AuthCredentialsDTO credentials, HttpServletResponse response);

}
