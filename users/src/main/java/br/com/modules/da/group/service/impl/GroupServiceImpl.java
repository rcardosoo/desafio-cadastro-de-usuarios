package br.com.modules.da.group.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import br.com.modules.da.exception.NotFoundException;
import br.com.modules.da.group.dto.GroupDTO;
import br.com.modules.da.group.model.Group;
import br.com.modules.da.group.repository.IGroupRepository;
import br.com.modules.da.group.service.IGroupResourceLinkingService;
import br.com.modules.da.group.service.IGroupService;
import br.com.modules.da.util.Constants;
import br.com.modules.da.util.UUIDUtils;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class GroupServiceImpl implements IGroupService {

	@NonNull private final IGroupRepository repository;
	@NonNull private final IGroupResourceLinkingService groupResourceLinkingService;

	@Override
	public GroupDTO create(GroupDTO groupDTO) {
		Group group = groupDTO.toEntity();
		
		group.setResources(groupResourceLinkingService
				.vinculateResourceEntityList(groupDTO.getResources()));
		
		return GroupDTO.toDto(repository.save(group));
	}

	@Override
	public GroupDTO getOne(String id) {
		UUID identifier = UUIDUtils.validateString(id);

		return repository.findById(identifier).map(GroupDTO::toDto)
				.orElseThrow(() -> new NotFoundException(Constants.NOT_FOUND_GROUP));
	}

	@Override
	public GroupDTO update(String id, GroupDTO groupDTO) {
		UUID identifier = UUIDUtils.validateString(id);

		return repository.findById(identifier).map(group -> {
			group.setName(groupDTO.getName());
			group.setStatus(groupDTO.getStatus());
			
			group.setResources(groupResourceLinkingService
					.vinculateResourceEntityList(groupDTO.getResources()));
			
			return GroupDTO.toDto(repository.save(group));
		}).orElseThrow(() -> new NotFoundException(Constants.NOT_FOUND_GROUP));
	}

	@Override
	public GroupDTO delete(String id) {
		UUID identifier = UUIDUtils.validateString(id);
		
		return repository.findById(identifier).map(group -> {
			repository.deleteById(identifier);
			return GroupDTO.toDto(group);
        }).orElseThrow(() -> new NotFoundException(Constants.NOT_FOUND_GROUP));
	}

	@Override
	public Page<GroupDTO> getAll(Pageable pageable) {
        List<GroupDTO> dtoList = new ArrayList<>();
		Page<Group> pageResult = repository.findAll(pageable);
		pageResult.forEach(group -> dtoList.add(GroupDTO.toDto(group)));
		return new PageImpl<>(dtoList, pageable, pageResult.getTotalElements());
	}

}
