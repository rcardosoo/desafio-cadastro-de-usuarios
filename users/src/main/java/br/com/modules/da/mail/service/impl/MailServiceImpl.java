package br.com.modules.da.mail.service.impl;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import org.thymeleaf.TemplateEngine;

import br.com.modules.da.mail.dto.HTMLMailDTO;
import br.com.modules.da.mail.service.IMailService;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
@RequiredArgsConstructor
public class MailServiceImpl implements IMailService {
	
    @NonNull private final JavaMailSender javaMailSender;
    @NonNull private final TemplateEngine htmlTemplateEngine;  
    
	@Override
	public void sendHTMLMail(HTMLMailDTO mail) {
		MimeMessage message = javaMailSender.createMimeMessage();
		
		final String htmlContent = this.htmlTemplateEngine
				.process(mail.getPagePath(), mail.getContextAttributes());
		
		try {
			MimeMessageHelper helper = new MimeMessageHelper(message, true, "UTF-8");
			helper.setText(htmlContent, true);
			helper.setTo(mail.getTo().stream().toArray(String[]::new));
			helper.setSubject(mail.getSubject());
	        javaMailSender.send(message);
		} catch(MessagingException e) {
			log.error("Send mail failed");
			log.error(e.getMessage());
		}
	}
	
}
