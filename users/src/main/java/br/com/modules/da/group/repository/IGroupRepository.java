package br.com.modules.da.group.repository;

import java.util.UUID;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.modules.da.group.model.Group;
	
@Repository
public interface IGroupRepository extends JpaRepository<Group, UUID> {
	
}
