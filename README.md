## Sistema cadastro de usuários


***

Para rodar o projeto é necessário a instalação prévia do Java, Maven, NPM, Node e Angular CLI

**Como executar**

* Acessar o diretório do backend (**/users**) e executar o comando:
```
> mvn spring-boot:run
```

O backend irá rodar na porta **8000**

* Acessar o diretório do frontend (**/users-frontend**) e executar os comandos:
```
> npm install && ng serve
```

**Testes**

Os testes de integração podem da aplicação serão executados no build ou utilizando o comando:
```
> mvn test
```

**Endpoints**

### Cadastrar usuário

POST - /users
```
{
    "email": "",
    "birthdate": "",
    "companyId": 0
}
```
> Retorna usuário cadastrado

### Atualizar usuário

PUT - /users/**{ ID_DO_USUÁRIO }**
```
{
    "email": "",
    "birthdate": "",
    "companyId": 0
}
```
> Retorna usuário atualizado

### Buscar usuário

GET - /users/**{ ID_DO_USUÁRIO }**

> Retorna usuário


### Listar usuários

GET - /users

> Retorna lista de usuários

### Deletar usuário

GET - /users/**{ ID_DO_USUÁRIO }**

> Retorna usuário removido