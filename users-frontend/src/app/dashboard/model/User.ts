export interface User {
    userId: number,
    email: string,
    birthdate: string,
    companyId: number
}