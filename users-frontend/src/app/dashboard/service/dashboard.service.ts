import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';
import { User } from '../model/User';

@Injectable({
  providedIn: 'root'
})
export class DashboardService {

  private apiUrl = environment.API + '/users';

  constructor(private http: HttpClient) {}

  obtainUserList(): Observable<User[]> {
    return this.http.get<User[]>(this.apiUrl);
  }

  create(user: User) {
    return this.http.post<User>(this.apiUrl, user);
  }

  update(user: User) {
    return this.http.put<User>(this.apiUrl + '/' + user.userId, user);
  }

  delete(user: User) {
    return this.http.delete<User>(this.apiUrl + '/' + user.userId);
  }
}
