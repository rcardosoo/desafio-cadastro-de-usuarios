import { Component, OnInit } from '@angular/core';
import { User } from './model/User';
import { DashboardService } from './service/dashboard.service';
import { FormBuilder, FormControl, Validators } from '@angular/forms';
import { tap, take, catchError } from 'rxjs/operators';
import { HttpErrorResponse } from '@angular/common/http';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { of } from 'rxjs';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ModalComponent } from '../modal/modal.component';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  users: User[];
  displayedColumns: string[] = [
    'email', 
    'birthdate', 
    'companyId',
    'action'
  ];

  constructor(private dashboardService: DashboardService,
    private snackBar: MatSnackBar,
    public dialog: MatDialog,
    private router: Router) { }

  ngOnInit(): void {
    this.dashboardService.obtainUserList().pipe(
      tap(
        (result: User[]) => {
          console.log(result);
          this.users = result;
        }
      ),
      catchError(
        (err: HttpErrorResponse) => {
          this.snackBar.open(`Um Erro Ocorreu: ${err.message}`, null, { duration: 4000 });
          console.log(err.error);
          this.router.navigateByUrl('');
          return of(err);
        }
      ),
      take(1)
    ).subscribe();
  }

  openDialog(action, obj): void {
    obj.action = action;
    const dialogRef = this.dialog.open(ModalComponent, {
      width: '300px',
      data: obj
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result.event == 'Cadastrar') {
        this.addUser(result.data);
      } else if(result.event == 'Atualizar') {
        this.updateUser(result.data);
      } else if(result.event == 'Remover') {
        this.deleteUser(result.data);
      }
    });
  }

  addUser(obj) {
    this.dashboardService.create(obj).pipe(
      tap(
        (result: User) =>{
          this.snackBar.open('O usuário foi cadastrado com sucesso', null, {duration: 4000});
          this.router.navigateByUrl('');
        }
      ),
      catchError((err: HttpErrorResponse) => {
        console.log(err.message);
        this.snackBar.open('Um Erro Ocorreu: ' + err.error, null, {duration: 4000});
        return of(err)
      }),
      take(1)
    ).subscribe();
  }

  updateUser(obj) {
    this.dashboardService.update(obj).pipe(
      tap(
        (result: User) =>{
          this.snackBar.open('O usuário foi atualizado com sucesso', null, {duration: 4000});
          this.router.navigateByUrl('');
        }
      ),
      catchError((err: HttpErrorResponse) => {
        console.log(err.message);
        this.snackBar.open('Um Erro Ocorreu: ' + err.error, null, {duration: 4000});
        return of(err)
      }),
      take(1)
    ).subscribe();
  }

  deleteUser(obj) {
    this.dashboardService.delete(obj).pipe(
      tap(
        (result: User) =>{
          this.snackBar.open('O usuário foi removido com sucesso', null, {duration: 4000});
          this.router.navigateByUrl('');
        }
      ),
      catchError((err: HttpErrorResponse) => {
        console.log(err.message);
        this.snackBar.open('Um Erro Ocorreu: ' + err.error, null, {duration: 4000});
        return of(err)
      }),
      take(1)
    ).subscribe();
  }

}
